# Friulian translation for gnome-nettool.
# Copyright (C) 2017 gnome-nettool's COPYRIGHT HOLDER
# This file is distributed under the same license as the gnome-nettool package.
# Fabio Tomat <f.t.public@gmail.com>, 2017.
#
msgid ""
msgstr ""
"Project-Id-Version: gnome-nettool master\n"
"POT-Creation-Date: 2017-09-22 18:56+0000\n"
"PO-Revision-Date: 2017-09-28 16:23+0200\n"
"Language-Team: Friulian <fur@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: fur\n"
"Last-Translator: Fabio Tomat <f.t.public@gmail.com>\n"
"X-Generator: Poedit 2.0.3\n"

#. Put one translator per line, in the form NAME <EMAIL>, YEAR1, YEAR2
msgctxt "_"
msgid "translator-credits"
msgstr "Fabio Tomat <f.t.public at gmail.com>, 2017"

#. (itstool) path: title/media
#. This is a reference to an external file such as an image or video. When
#. the file changes, the md5 hash will change to let you know you need to
#. update your localized copy. The msgstr is not used at all. Set it to
#. whatever you like once you have updated your copy of the file.
#: C/index.page:5
msgctxt "_"
msgid ""
"external ref='figures/gnome-nettool-trail.png' "
"md5='fa7ce8464985fc4e631454c81b873d46'"
msgstr ""
"external ref='figures/gnome-nettool-trail.png' "
"md5='fa7ce8464985fc4e631454c81b873d46'"

#. (itstool) path: info/title
#: C/index.page:6
msgctxt "link"
msgid "Net Tool"
msgstr "Strument di rêt"

#. (itstool) path: info/title
#: C/index.page:7
msgctxt "text"
msgid "Net Tool"
msgstr "Strument di rêt"

#. (itstool) path: credit/name
#: C/index.page:11 C/introduction.page:11 C/ip-information.page:10
#: C/network-connectivity.page:10
msgid "Germán Poo-Caamaño"
msgstr "Germán Poo-Caamaño"

#. (itstool) path: credit/years
#: C/index.page:13 C/introduction.page:13 C/ip-information.page:12
#: C/network-connectivity.page:12
msgid "2012"
msgstr "2012"

#. (itstool) path: info/desc
#: C/index.page:16
msgid "A network diagnostic tool"
msgstr "Un strument di diagnostiche di rêt"

#. (itstool) path: license/p
#: C/index.page:18 C/introduction.page:16 C/ip-information.page:15
#: C/network-connectivity.page:15
msgid "Creative Commons Share Alike 3.0"
msgstr "Creative Commons Condivision tal stes mût 3.0"

#. (itstool) path: title/media
#. This is a reference to an external file such as an image or video. When
#. the file changes, the md5 hash will change to let you know you need to
#. update your localized copy. The msgstr is not used at all. Set it to
#. whatever you like once you have updated your copy of the file.
#: C/index.page:22
msgctxt "_"
msgid ""
"external ref='figures/gnome-nettool.png' "
"md5='a288cc42fc2a8fb341f63893e4dd557e'"
msgstr ""
"external ref='figures/gnome-nettool.png' "
"md5='a288cc42fc2a8fb341f63893e4dd557e'"

#. (itstool) path: page/title
#: C/index.page:22
msgid ""
"<media type=\"image\" src=\"figures/gnome-nettool.png\">Net Tool logo</"
"media>Net Tool"
msgstr ""
"<media type=\"image\" src=\"figures/gnome-nettool.png\">Logo strument di "
"rêt</media>Strument di rêt"

#. (itstool) path: section/title
#: C/index.page:25
msgid "Getting Network Information"
msgstr "Daûr a otignî informazions su la rêt"

#. (itstool) path: info/desc
#: C/introduction.page:7
msgid "Introduction to the <em>Net Tool</em>."
msgstr "Introduzion al <em>Strument di rêt</em>."

#. (itstool) path: page/title
#: C/introduction.page:20
msgid "Introduction"
msgstr "Introduzion"

#. (itstool) path: page/p
#: C/introduction.page:22
msgid ""
"<app>Nettool</app> is a tool to diagnose the network and services status. It "
"provides a graphical user interface for a number of common network tools, "
"such as ping, netstat, traceroute, port scan, lookup, finger, and whois."
msgstr ""
"<app>Nettool</app> al è un strument pe diagnosi dal stât de rêt e dai "
"servizis. Al furnìs une interface utent grafiche par un ciert numar "
"struments di rêt come ping, netstat, traceroute, port scan, lookup, finger e "
"whois."

#. (itstool) path: page/p
#: C/introduction.page:27
msgid ""
"The functionality is very basic, it is oriented to get the first insight of "
"an potential issue and to bring this information to technical staff."
msgstr ""
"Il funzionament al è une vore sempliç, al è orientât a vê un prin indizi "
"suntun probleme potenziâl e par puartâ chestis informazions al personâl "
"tecnic."

#. (itstool) path: info/desc
#: C/ip-information.page:6
msgid "Find the Internet Address (IP) that provides Internet connectivity."
msgstr ""
"Cjate la direzion di internet (IP) che e furnìs la conetivitât a internet."

#. (itstool) path: page/title
#: C/ip-information.page:19
msgid "Getting the Internet Address"
msgstr "Daûr a otignî la direzion di internet"

#. (itstool) path: page/p
#: C/ip-information.page:21
msgid ""
"Usually a computer has one or more network devices. However, one of them "
"provides connectivity to Internet. In <gui>Devices</gui> you can query the "
"information for each device and get some usage statistics. The statistics "
"are set to zero any time you restart your computer."
msgstr ""
"Di solit un computer al à un o plui dispositîfs di rêt. Dut câs, dome un di "
"chei al furnìs la conetivitât a internet. In <gui>Dispositîfs</gui> si pues "
"consultâ lis informazions di ogni dispositîf e otignî cualchi statistiche di "
"ûs. Lis statistichis a tachin di zero ogni volte che si torne a inviâ il "
"computer."

#. (itstool) path: page/p
#: C/ip-information.page:26 C/network-connectivity.page:23
msgid ""
"You can query the information of a network device by clicking next to "
"<gui>Network Device</gui>."
msgstr ""
"Al è pussibil consultâ lis informazions di un dispositîf di rêt fasint clic "
"in bande a <gui>Dispositîf di rêt</gui>."

#. (itstool) path: info/desc
#: C/network-connectivity.page:6
msgid "Determine if your computer has Internet access."
msgstr "Determine se il computer al à l'acès a internet."

#. (itstool) path: page/title
#: C/network-connectivity.page:19
msgid "Checking Network Connectivity"
msgstr "Daûr a controlâ la conetivitât de rêt"

#. (itstool) path: page/p
#: C/network-connectivity.page:21
msgid "Select the <gui>Ping</gui> tab to check the network connectivity."
msgstr ""
"Selezione la schede <gui>Ping</gui> par controlâ la conetivitât de rêt."

#. (itstool) path: page/p
#: C/network-connectivity.page:26
msgid ""
"If you successfully get replies from another machine in Internet, then your "
"computer is connected to a network (likely to Internet as well). This helps "
"you to discard an issue in your computer for the lack of connectivity to "
"other sites."
msgstr ""
"Se si è bogns di ricevi rispuestis di une altre machine in internet, alore "
"il computer al è conetût a une rêt (e duncje ancje a internet). Chest al "
"jude a scartâ problemis tal computer pe mancjance di conetivitât a altris "
"sîts."
